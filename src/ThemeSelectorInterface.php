<?php

namespace Drupal\theme_selector;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Theme Selector entity.
 */
interface ThemeSelectorInterface extends ConfigEntityInterface {


  /**
   * Node title.
   *
   * @var string
   */

  /**
   * Returns the entity title.
   *
   * @return string
   *   The entity title.
   */
  public function getTitle();

  /**
   * Sets the entity title.
   *
   * @param string $title
   *   Node title.
   *
   * @return $this
   *   The Theme Selector entity.
   */
  public function setTitle($title);

  /**
   * Returns the entity suffix.
   *
   * @return string
   *   The entity suffix.
   */
  public function getSuffix();

  /**
   * Sets the entity suffix.
   *
   * @param string $suffix
   *   Entity Suffix.
   *
   * @return $this
   *   The Theme Selector entity.
   */
  public function setSuffix($suffix);

  /**
   * Returns the entity theme.
   *
   * @return string
   *   The entity theme.
   */
  public function getTheme();

  /**
   * Sets the entity theme.
   *
   * @param string $theme
   *   Entity Theme.
   *
   * @return $this
   *   The Theme Selector entity.
   */
  public function setTheme($theme);

  /**
   * Show a message accordingly to status, after creating/updating an entity.
   *
   * @param int $status
   *   Status int, returned after creating/updating an entity.
   */
  public function statusMessage($status);

}
