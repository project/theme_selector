<?php

namespace Drupal\theme_selector\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a list of Theme Selectors entities.
 */
class ThemeSelectorListBuilder extends ConfigEntityListBuilder {

  const TITLE = 'title';

  /**
   * Constructs the table header.
   *
   * @return array
   *   Table header
   */
  public function buildHeader() {
    $header[self::TITLE] = $this->t('Title');
    return $header + parent::buildHeader();
  }

  /**
   * Constructs the table rows.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   *
   * @return \Drupal\Core\Entity\EntityListBuilder
   *   A render array structure of fields for this entity.
   */
  public function buildRow(EntityInterface $entity) {

    $row[self::TITLE] = $entity->get('title') . ' (' . $entity->id() . ')';

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity, $type = 'edit') {
    $operations = parent::getDefaultOperations($entity);
    $operations['edit'] = [
      self::TITLE => $this->t('Edit'),
      'weight' => 0,
      'url' => Url::fromRoute('entity.theme_selector.edit_form', ['theme_selector' => $entity->id()]),
    ];
    $operations['delete'] = [
      self::TITLE => $this->t('Delete'),
      'weight' => 0,
      'url' => Url::fromRoute('entity.theme_selector.delete_form', ['theme_selector' => $entity->id()]),
    ];

    return $operations;
  }

}
