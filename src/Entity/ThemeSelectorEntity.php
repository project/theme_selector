<?php

namespace Drupal\theme_selector\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\theme_selector\ThemeSelectorInterface;

/**
 * Defines the Theme Selector entity.
 *
 * @ConfigEntityType(
 *   id = "theme_selector",
 *   label = @Translation("Theme Selector"),
 *   handlers = {
 *    "list_builder" = "Drupal\theme_selector\Controller\ThemeSelectorListBuilder",
 *    "form" = {
 *       "add-form" = "Drupal\theme_selector\Form\ThemeSelectorForm",
 *       "edit-form" = "Drupal\theme_selector\Form\ThemeSelectorForm",
 *       "delete-form" = "Drupal\theme_selector\Form\ThemeSelectorDeleteForm",
 *      },
 *    },
 *   config_prefix = "theme_selector",
 *   admin_permission = "administer theme selector",
 *   entity_keys = {
 *     "title" = "title",
 *     "id" = "id",
 *     "suffix" = "suffix",
 *     "theme" = "theme"
 *   },
 *   config_export = {
 *     "id",
 *     "title",
 *     "suffix",
 *     "theme",
 *   }
 * )
 */
class ThemeSelectorEntity extends ConfigEntityBase implements ThemeSelectorInterface {

  use StringTranslationTrait, MessengerTrait;

  /**
   * Theme Selector entity id.
   *
   * @var string
   */
  protected $id;

  /**
   * Theme Selector entity title.
   *
   * @var string
   */
  protected $title;

  /**
   * Node title.
   *
   * @var string
   */

  /**
   * Returns the entity title.
   *
   * @return string
   *   The entity title.
   */
  public function getTitle() {
    return $this->get('title');
  }

  /**
   * Sets the entity title.
   *
   * @param string $title
   *   Node title.
   *
   * @return $this
   *   The Theme Selector entity.
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * Returns the entity title.
   *
   * @return string
   *   The entity title.
   */
  public function getSuffix() {
    return $this->get('id');
  }

  /**
   * Sets the entity title.
   *
   * @param string $suffix
   *   The Suffix for the theme.
   *
   * @return $this
   *   The Theme Selector entity.
   */
  public function setSuffix($suffix) {
    $this->set('id', $suffix);
    return $this;
  }

  /**
   * Returns the entity title.
   *
   * @return string
   *   The entity theme.
   */
  public function getTheme() {
    return $this->get('theme');
  }

  /**
   * Sets the entity title.
   *
   * @param string $theme
   *   The theme chosen.
   *
   * @return $this
   *   The Theme Selector entity.
   */
  public function setTheme($theme) {
    $this->set('theme', $theme);
    return $this;
  }

  /**
   * Show a message accordingly to status, after creating/updating an entity.
   *
   * @param int $status
   *   Status int, returned after creating/updating an entity.
   */
  public function statusMessage($status) {
    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label entity.', ['%label' => $this->getTitle()]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label entity was not saved.', ['%label' => $this->getTitle()]));
    }
  }

}
