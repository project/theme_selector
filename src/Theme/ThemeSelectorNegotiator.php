<?php

namespace Drupal\theme_selector\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\theme_selector\ThemeSelectorHelper;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The class ThemeSelectorNegotiator implements the ThemeNegotiatorInterface.
 */
class ThemeSelectorNegotiator implements ThemeNegotiatorInterface {


  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Class construct.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   */
  public function __construct(RequestStack $request_stack) {

    $this->requestStack = $request_stack;
  }

  /**
   * Validates if it is ok to apply the theme.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The RouteMatch.
   *
   * @return bool
   *   Return True if condition applies.
   */
  public function applies(RouteMatchInterface $route_match) {
    $query_theme = $this->requestStack->getCurrentRequest()->get('theme-selector');
    if ($query_theme) {

      // Get array of possible themes to apply.
      $themeSelector = ThemeSelectorHelper::getThemeSelector($query_theme);
      if ($themeSelector) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * This functions returns the name of the theme to be applied.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The RouteMatch.
   *
   * @return string
   *   Returns the chosen theme.
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    // Get applyTheme and appliedTheme from session.
    $query_theme = $this->requestStack->getCurrentRequest()->get('theme-selector');
    if ($query_theme) {
      // Get array of possible themes to apply.
      $themeSelector = ThemeSelectorHelper::getThemeSelector($query_theme);
      if ($themeSelector) {
        return $themeSelector->getTheme();
      }
    }
  }

}
