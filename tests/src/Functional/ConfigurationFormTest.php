<?php

namespace Drupal\Tests\theme_selector\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group theme_selector.
 */
class ConfigurationFormTest extends BrowserTestBase {

  /**
   * Module Variable.
   *
   * @var array
   */
  protected static $modules = [
    'theme_selector',
  ];

  /**
   * Default Theme.
   *
   * @var string
   */
  protected $defaultTheme = 'claro';

  /**
   * Set up the test.
   */
  protected function setUp(): void {
    parent::setUp();

    $account =
            $this->drupalCreateUser([
              'administer theme selector',
            ]);

    $this->drupalLogin($account);
  }

  /**
   * Test Module not breaking site.
   */
  public function testModuleDoesNotBreaksSite() {
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test Accessing the content page.
   */
  public function testCanAccessContentMessages() {
    $this->drupalGet('/admin/config/user-interface/theme-selector');
    $this->assertSession()->pageTextContains('Theme Selector');
  }

}
