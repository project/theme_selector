<?php

namespace Drupal\theme_selector\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Theme Selector Form.
 */
class ThemeSelectorForm extends EntityForm {
  /**
   * Drupal\Core\Plugin\DefaultPluginManager definition.
   *
   * @var Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Drupal\Core\Plugin\DefaultPluginManager definition.
   *
   * @var Drupal\Core\Entity\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityQuery;

  /**
   * Class Constructor.
   */
  public function __construct(ThemeHandlerInterface $themeHandler, EntityTypeManagerInterface $entityQuery) {
    $this->themeHandler = $themeHandler;
    $this->entityQuery = $entityQuery;
  }

  /**
   * Class Create.
   *
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('theme_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns list of installed themes.
   *
   * @return array
   *   Return the installed themes.
   */
  public function getInstalledThemes() {
    return $this->themeHandler->listInfo();
  }

  /**
   * Get Form Id.
   *
   * (@inheritdoc).
   */
  public function getFormId() {
    return "theme_selector_form";
  }

  /**
   * Build Form.
   *
   * (@inheritdoc)
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    // Get list of installed themes.
    $installed_themes = $this->getInstalledThemes();

    $form['title'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->getTitle(),
      '#size' => 25,
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Suffix'),
      '#default_value' => $this->entity->id(),
      '#disabled' => !$this->entity->isNew(),
      '#description' => $this->t('Unique Suffix to identify this Theme Selector.'),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
    ];

    $form['theme'] = [
      '#title' => $this->t('Name of the theme to implement'),
      '#type' => 'select',
      '#default_value' => $this->entity->getTheme(),
      '#required' => TRUE,
      '#options' => array_combine(array_keys($installed_themes), array_keys($installed_themes)),
    ];

    return $form;
  }

  /**
   * Validate Form.
   *
   * (@inheritdoc).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $theme = $form_state->getValue('theme');
    $suffix = $form_state->getValue('suffix');
    $query = $this->entityTypeManager->getStorage('theme_selector')->getQuery();
    $entity_theme = $query->condition('theme', $theme)
      ->accessCheck(TRUE)
      ->execute();

    if (!empty($entity_theme)) {
      $form_state->setErrorByName('theme', $this->t('There already exists a suffix for this theme.',
            ['%theme' => $theme, '%suffix' => $suffix]
        ));
    }

  }

  /**
   * Submit Form.
   *
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $status = $this->entity->save();
    $this->entity->statusMessage($status);
    $form_state->setRedirect('entity.theme_selector.list');
  }

  /**
   * Function to check whether an theme selector entity exists.
   */
  public function exist($id) {
    $query = $this->entityTypeManager->getStorage('theme_selector')->getQuery();
    $entity = $query->condition('theme', $id)
      ->accessCheck(TRUE)
      ->execute();
    return (bool) $entity;
  }

}
