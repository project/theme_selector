## CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

## INTRODUCTION
------------

This simple module allows you to change the theme of a page with
just a query string, using Theme Negotiator.

## REQUIREMENTS
------------

This module does not have any special requirements

## INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

## CONFIGURATION
-------------

* Install Theme Selector module using composer and enable it.
* Go to /admin/config/user-interface/theme-selector and add a Theme Selector.
* Select the Theme and the associated suffix.
* Using a query string theme-selector you'll be able to change the page theme.
* Example:
* https://example.com/?theme-selector=bartik

## MAINTAINERS
-----------

 * Current maintainers: [nsalves](https://www.drupal.org/u/nsalves)

This project has been sponsored by:
 * NTT DATA
    NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
    NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
    NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
    We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate