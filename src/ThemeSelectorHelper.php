<?php

namespace Drupal\theme_selector;

/**
 * Helper for Theme Selector List.
 */
class ThemeSelectorHelper {

  /**
   * Query for theme selectors.
   *
   * @return array
   *   Return the List of Theme Selectors.
   */
  public static function getThemeSelector(string $theme_selector) {
    $theme_selector_bundle = \Drupal::entityTypeManager()->getStorage('theme_selector');
    $theme_selector_entity = $theme_selector_bundle->load($theme_selector);
    return $theme_selector_entity;
  }

}
